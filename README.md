# lein-emacs

Dockerfile for emacs with config from [Clojure for the Brave and True](http://www.braveclojure.com/basic-emacs/#Configuration).

## Usage

The instructions below use the Docker-image that has the exact setup form the book. For other variants see <https://gitlab.com/eval/docker-lein-emacs/container_registry>.

```bash
# current folder is your project:
$ docker run -it --rm --name project -v $(pwd):/root/project registry.gitlab.com/eval/docker-lein-emacs:default

# then open file:
C-x C-f path/to/open.clj
# ...and start nrepl:
M-x cider-jack-in
```

Recommended usage:

```bash
# in e.g. ~/.bashrc

function demacs() {
  path=$(pwd)
  name=${path##*/}
  docker run -it --rm --name "emacs-${name}" -v ${path}:/root/project registry.gitlab.com/eval/docker-lein-emacs:default emacs "/root/project/${1}"
}

# usage:
# within a lein-project:
# $ demacs src/euler/core.clj

```

## Development

See `.gitlab-ci.yml` for creation of the dockers.